//$or operator

db.users.find(
	{ $or: [ { firstName: {$regex: "s", $options: "$i"}}, { lastName: {$regex: "d", $options: "$i"}} ] }, 
	{
		_id: 0,
		age:0,
		contact: 0,
		courses:0,
		department:0
	}
	);

//$and operator

db.users.find({ $and: [{department: "HR"}, {age: {$gte: 70} }] });

//$and, $regex, $lte operators

db.users.find({ $and: [{firstName: {$regex: "e", $options: "$i"}}, {age: {$lte: 30} }] });
